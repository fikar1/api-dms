<?php

namespace App\Controllers;

use App\Entities\Collection;
use App\Entities\User;
use App\Models\GroupModel;
use App\Models\PermissionModel;
use App\Models\SiupModel;
use App\Models\UserModel;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\RESTful\ResourceController;
use Config\Services;
use Exception;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class UserController extends ResourceController
{
    use ResponseTrait;

    protected $auth;

    /**
     * @var AuthConfig
     */
    protected $config;

    /**
     * @var Session
     */
    protected $session;

    protected  $authorize;

    public function __construct()
    {
        // Most services in this controller require
        // the session to be started - so fire it up!
        $this->session = service('session');

        $this->config = config('Auth');
        $this->auth   = service('authentication');

        $this->authorize = service('authorization');
        $this->users = new UserModel();
        $this->group = new GroupModel();
        $this->permission = new PermissionModel();
        $this->siup = new SiupModel();
        helper('Auth');
    }

    public function details()
    {
        $key = getenv('TOKEN_SECRET');
        $header = $this->request->getServer('HTTP_AUTHORIZATION');
        if (!$header) return Services::response()
            ->setJSON(['message' => 'Token Required Details'])
            ->setStatusCode(ResponseInterface::HTTP_UNAUTHORIZED);
        $token = explode(' ', $header)[1];

        try {
            $use = new User();
            $decoded = JWT::decode($token, new Key($key, "HS256"));

            if ($decoded) {

                $response = [
                    'status' => 200,
                    'error' => false,
                    'messages' => 'User details',
                    'data' => [
                        'profile' => $decoded,
                        "siup" => $this->siup->getSiup(),
                    ]
                ];
                return $this->respondCreated($response);
            }
        } catch (Exception $ex) {
            $response = [
                'status' => 401,
                'error' => true,
                'messages' => 'Access denied Details',
                'data' => []
            ];
            return $this->respondCreated($response);
        }
    }
    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    public function index()
    {

        // if (!hasPermission('user_manage')) {
        //     return $this->respond([
        //         'message' => 'No access'
        //     ], 403);
        // }

        // $start = $this->request->getGet('start');
        // $length = $this->request->getGet('length');
        // $search = $this->request->getGet('search');

        // return $this->respond(Collection::datatable(
        //     $this->users->getResource($search)->limit($length, $start)->get()->getResultObject(),
        //     $this->users->getResource()->countAllResults(),
        //     $this->users->getResource($search)->countAllResults()
        // ));
    }

    /**
     * Return the properties of a resource object
     *
     * @return mixed
     */
    public function show($id = null)
    {
        //
        if (!hasPermission('profile')) {
            return $this->respond([
                'code' => 403,
                'message' => 'No access'
            ], 403);
        }

        $obj = new \stdClass();
        $obj->status = true;
        $obj->message = '';

        $userModel = new UserModel();
        $user = $userModel->withProfile($id);
        $obj->data = array(
            "id" =>  $user->id,
            "email" =>  $user->email,
            "alamat" =>  $user->alamat,
            "hp" =>  $user->nomor_hp,

            "full_name" =>  $user->full_name,
            "username" =>  $user->username,
            "avatar" =>  base_url() . '/' . $user->uri_path . '/' . $user->avatar,
            "updated_at" =>  $user->updated_at,
            "active" => $user->active,
        );
        if ($user) {
            $obj->status = 200;
            $obj->error = false;
            $obj->data;
        } else {

            $obj->status = 500;
            $obj->error = true;
            $obj->data = $user;
        }

        return $this->response->setJSON($obj);
    }

    /**
     * Add or update a model resource, from "posted" properties
     *
     * @return mixed
     */
    public function update($id = null)
    {
        //
        if (!hasPermission('profile')) {
            return $this->respond([
                'code' => 403,
                'status' => false,
                'message' => 'You don\'t have permission to access the data'
            ], 403);
        }
        $obj = new \stdClass();
        $obj->status = true;
        $obj->message = '';

        if ($this->request->getMethod() === 'post') {
            $id = getUserid();
            $validationRules = [
                'email'        => "required",
                'username'     => "required|alpha_numeric_space|min_length[3]",
            ];

            if (!$this->validate($validationRules)) {
                $obj->status = false;
                $obj->message = $this->validator->getErrors();
                return $this->response->setJSON($obj);
            }

            $user = new User();


            if ($this->request->getVar('password')) {

                $validationRules = [
                    'password'     => 'if_exist',
                    'pass_confirm' => 'matches[password]',
                ];

                if (!$this->validate($validationRules)) {
                    $obj->status = false;
                    $obj->message = $this->validator->getErrors();
                    return $this->response->setJSON($obj);
                }

                $user->password = $this->request->getVar('password');
            }


            if ($this->request->getFile('userfile')) {
                $this->validate([
                    'userfile' => 'uploaded[userfile]|max_size[userfile,100]'
                        . '|mime_in[userfile,image/png,image/jpg,image/gif]'
                        . '|ext_in[userfile,png,jpg,gif]|max_dims[userfile,1024,768]'
                ]);
                $file = $this->request->getFile('userfile');

                if (!$path = $file->store()) {
                    return $this->respond([
                        'message' => 'Error occurred while uploading the image'
                    ], 500);
                }

                $result = uploadImages($path, 'users');

                if (!$result) {
                    return $this->respond([
                        'message' => 'Error occurred while uploading the image'
                    ], 500);
                }

                $user->avatar = $result['medium'];
                $user->uri_path = $result['uri_path'];
            }


            $user->email = $this->request->getVar('email');
            $user->username = $this->request->getVar('username');
            $user->full_name = $this->request->getVar('full_name');
            if ($this->users->skipValidation(true)->update(getUserid(), $user)) {
                $obj->status = true;
                $obj->message = 'Update Profile Success';
                return $this->response->setJSON($obj);
            }

            $obj->status = false;
            $obj->message = 'Failed to Update';
            return $this->response->setJSON($obj);
        }
    }

    /**
     * Add or update a model resource, from "posted" properties
     *
     * @return mixed
     */
    public function updatePassword($id = null)
    {
        //
        if (!hasPermission('profile')) {
            return $this->respond([
                'code' => 403,
                'status' => false,
                'message' => 'You don\'t have permission to access the data'
            ], 403);
        }
        $obj = new \stdClass();
        $obj->status = true;
        $obj->message = '';

        if ($this->request->getMethod() === 'post') {
            $id = getUserid();
            $user = new User();


            if ($this->request->getVar('password')) {

                $validationRules = [
                    'password'     => 'if_exist',
                    'pass_confirm' => 'matches[password]',
                ];

                if (!$this->validate($validationRules)) {
                    $obj->status = false;
                    $obj->message = $this->validator->getErrors();
                    return $this->response->setJSON($obj);
                }

                $user->password = $this->request->getVar('password');
            }
            if ($this->users->skipValidation(true)->update(getUserid(), $user)) {
                $obj->status = true;
                $obj->message = 'Update Password Success';
                return $this->response->setJSON($obj);
            }

            $obj->status = false;
            $obj->message = 'Failed to Update Password';
            return $this->response->setJSON($obj);
        }
    }


    /**
     * Delete the designated resource object from the model
     *
     * @return mixed
     */
    public function delete($id = null)
    {
        //
    }

    public function comboVerifikatorLayanan()
    {
        $userGroup = $this->group->getUsersForGroup(7);
        $obj = new \stdClass();
        $obj->status = true;
        $obj->message = '';
        $obj->data = $userGroup;
        return $this->response->setJSON($obj);
    }
}
