<?php

namespace App\Controllers;

use App\Entities\User;
use App\Libraries\Api;
use App\Libraries\Captcha;
use App\Models\GroupModel;
use App\Models\PermissionModel;
use App\Models\UserModel;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\RESTful\ResourceController;
use Exception;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use GuzzleHttp\Client;
use GuzzleHttp\Command\ServiceClient;

class AuthApiController extends ResourceController
{
    use ResponseTrait;

    protected $auth;

    /**
     * @var AuthConfig
     */
    protected $config;

    /**
     * @var Session
     */
    protected $session;

    protected $api;

    protected  $authorize;

    public function __construct()
    {
        // Most services in this controller require
        // the session to be started - so fire it up!
        $this->session = service('session');

        $this->config = config('Auth');
        $this->auth   = service('authentication');

        $this->authorize = service('authorization');

        $this->api = new Api();
        $this->group = new GroupModel();
        $this->permission = new PermissionModel();
        // $this->alokasi = new AlokasiModel();
        // $this->inkbli = new AlokasiKbliModel();
        // $this->inPangkalan = new AlokasiPelabuhanPangkalanModel();
        // $this->inPelabuhanMuat = new AlokasiPelabuhanMuatModel();
        // $this->inPelabuhanTujuan = new AlokasiPelabuhanTujuanModel();
        // $this->siup = new SiupModel();
        // $this->userProfile = new UserProfile();
        // $this->inDpi = new AlokasiDpiModel();

        // $this->uploadPath = WRITEPATH . 'uploads/';
        // $this->siupPath = ROOTPATH . 'public/uploads/siup/';

        $this->db = db_connect();
        helper('Auth');
    }
    public function login()
    {
        $rules = [
            "email" => "required|min_length[6]",
            "password" => "required",
        ];

        $message = [
            "email" => [
                "required" => "Email required",
                "valid_email" => "Email address is not in format"
            ],
            "password" => [
                "required" => "password is required"
            ],
        ];

        if (!$this->validate($rules, $message)) {

            $response = [
                'status' => 500,
                'error' => true,
                'message' => $this->validator->getErrors(),
                'data' => []
            ];

            return $this->respond($response, 500);
        } else {
            $userModel = new UserModel();

            // $userdata = $userModel->where("email", $this->request->getVar("email"))->first()->with('role_ids');
            $login = $this->request->getVar("email");
            // $userdata = $userModel->select('users.*, user_profile.full_name')->where("email", $login)->join('user_profile','users.id=user_profile.user_id','left')->first();

            // if (!empty($userdata)) {

            $this->auth = service('authentication');

            // $type = "email";
            // Determine credential type
            $type = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'nomor_hp';

            if ($this->auth->attempt([$type => $login, 'password' => $this->request->getVar("password")], false)) {

                $userdata = $userModel->select('users.*')->where("email", $login)->first();
                $key = getenv('TOKEN_SECRET');

                // getSessionID($userdata->id);

                $iat = time(); // current timestamp value
                $nbf = $iat + 10;
                $exp = $iat + 3600 * 24;

                $payload = array(
                    "iss" => "The_claim",
                    "aud" => "The_Aud",
                    "iat" => $iat, // issued at
                    "nbf" => $nbf, //not before in seconds
                    "exp" => $exp, // expire time in seconds
                    "is_login" => true,
                    "data" => array(
                        "id" => $userdata->id,
                        "email" => $userdata->email,
                        "username" => $userdata->username,
                        "full_name" => $userdata->full_name,
                        "sessionID" => session_id(),
                        "avatar" => base_url() . '/' . $userdata->uri_path . '/' . $userdata->avatar,
                        "roles" => $this->group->getGroupsForUser($userdata->id),
                        'permission' => $this->permission->getPermissionsForUsers($userdata->id),
                    ),
                );

                $token = JWT::encode($payload, $key, 'HS256');
                $response = [
                    'status' => 200,
                    'error' => false,
                    'message' => 'User logged In successfully',
                    'data' => [
                        'token' => $token
                    ]
                ];
                return $this->respondCreated($response);
            } else {

                $response = [
                    'status' => 400,
                    'error' => true,
                    'message' => 'Incorrect details',
                    'data' => []
                ];
                return $this->respond($response, 400);
            }
            // } else {
            //     $response = [
            //         'status' => 400,
            //         'error' => true,
            //         'message' => 'User not found',
            //         'data' => []
            //     ];
            //     return $this->respond($response, 400);
            // }
        }
    }

    public function logout()
    {

        session_destroy();
        $user_id = $this->request->getVar('user_id');
        return connectionID($user_id, 0);
    }

    public function registerBkp()
    {

        $users = model(UserModel::class);

        // Validate basics first since some password rules rely on these fields
        $rules = config('Validation')->registrationRules ?? [
            'username' => 'required|alpha_numeric_space|min_length[3]|max_length[30]|is_unique[users.username]',
            'email'    => 'required|valid_email|is_unique[users.email]',
        ];

        if (!$this->validate($rules)) {
            $response = [
                'status' => 400,
                'error' => true,
                'message' => $this->validator->getErrors(),
                'data' => []
            ];
            return $this->respond($response, 400);
            // return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
        }

        $rules = [
            'password'     => 'required',
            'pass_confirm' => 'required|matches[password]',
        ];

        if (!$this->validate($rules)) {
            $response = [
                'status' => 400,
                'error' => true,
                'message' => $this->validator->getErrors(),
                'data' => []
            ];
            return $this->respond($response, 400);
        }

        // Save the user
        $allowedPostFields = array_merge(['password'], $this->config->validFields, $this->config->personalFields);
        $user              = new User($this->request->getPost($allowedPostFields));

        $this->config->requireActivation === null ? $user->activate() : $user->generateActivateHash();
        // Ensure default group gets assigned if set
        if (!empty($this->config->defaultUserGroup)) {
            $users = $users->withGroup($this->config->defaultUserGroup);
        }

        if (!$users->save($user)) {
            $response = [
                'status' => 400,
                'error' => true,
                'message' => $users->errors(),
                'data' => []
            ];
            return $this->respond($response, 400);
        }

        if ($this->config->requireActivation !== null) {
            $activator = service('activator');
            $sent      = $activator->send($user);

            if (!$sent) {
                $response = [
                    'status' => 400,
                    'error' => true,
                    'message' => lang('Auth.unknownError'),
                    'data' => []
                ];
                return $this->respond($response, 400);
            }

            // Success!
            $response = [
                'status' => 200,
                'error' => true,
                'message' => lang('Auth.activationSuccess'),
                'data' => []
            ];
            return $this->respondCreated($response);
        }
    }

    public function register()
    {

        $siup = $this->request->getVar('siup');
        $tipe = $this->request->getVar('tipe');
        $nib = $this->request->getVar('nib');
        $nama_pemilik = $this->request->getVar('nama_pemilik');

        $captcha = new Captcha();
        $status = $captcha->store($this->request->getVar('g-recaptcha-response'));
        if ($status) {
            $rules = [
                'username' => 'required|min_length[3]|max_length[30]|is_unique[users.username]',
                'email'    => 'required|valid_email|is_unique[users.email]',
                'nomor_hp'    => 'required|is_unique[users.nomor_hp]',
                'password'     => 'required',
                'pass_confirm' => 'required|matches[password]',
            ];

            if (!$this->validate($rules)) {
                $response = [
                    'code' => 400,
                    'error' => true,
                    'message' => $this->validator->getErrors(),
                    'data' => null
                ];
                return $this->response->setJSON($response);
            }

            if ($tipe == 'nelayan_kecil') {
                $nib = [
                    'nib' => 'required',
                    'nama_pemilik' => 'required',
                ];
            } else {
                $nib = [
                    'siup' => ['label' => 'SIUP Required', 'rules' => 'required'],
                ];
            }

            if (!$this->validate($nib)) {
                $response = [
                    'code' => 400,
                    'error' => true,
                    'message' => $this->validator->getErrors(),
                    'data' => null
                ];
                return $this->respond($response, 400);
            }

            $this->db->transBegin();

            try {
                // code...
                $users = model(UserModel::class);
                // Save the user
                $allowedPostFields = array_merge(['password'], $this->config->validFields, $this->config->personalFields, ['nomor_hp']);
                $user              = new User($this->request->getPost($allowedPostFields));

                $this->config->requireActivation === null ? $user->activate() : $user->generateActivateHash();
                // Ensure default group gets assigned if set
                if (!empty($this->config->defaultUserGroup)) {
                    $users = $users->withGroup($this->config->defaultUserGroup);
                }

                $users->insert($user);
                $idUser = $users->insertID;

                if (!$idUser) {
                    $response = [
                        'code' => 400,
                        'error' => true,
                        'message' => $users->errors(),
                        'data' => []
                    ];
                    return $this->respond($response, 400);
                }

                // $idUser = 89;
                if ($tipe == 'nelayan_kecil') {
                    $datas = cekNib($nib, $nama_pemilik)['data'];
                } else {
                    $datas = cekSiup($siup)->data;
                }

                foreach ($datas as $key => $value) {
                    # code...
                    $provinsis = $value->provinsi_ok != null ? $value->provinsi_ok : 'not found';
                    $kabupatens = $value->kabupaten_kota != null ? $value->kabupaten_kota : 'not found';
                    $kecamatans = $value->kecamatan != null ? $value->kecamatan : 'not found';
                    $desa = $value->desa != null ? $value->desa : 'not found';

                    $prov_id = checkProvinsi($provinsis);

                    if ($prov_id) {
                        $kab_id = checkKabupaten($kabupatens, $prov_id);
                        if ($kab_id) {
                            $kec_id = checkKecamatan($kecamatans, $prov_id, $kab_id);
                            if ($kec_id) {
                                $desa_id = checkKelurahan($desa, $prov_id, $kab_id, $kec_id);
                            }
                        }
                    } else {
                        $response = [
                            'code' => 400,
                            'error' => true,
                            'message' => "Wilayah Not Found",
                            'data' => null
                        ];
                        return $this->response->setJSON($response);
                    }

                    $dataUser = array(
                        'user_id' => $idUser,
                        'idprovinsi' => $prov_id,
                        'id_kabupaten' => $kab_id,
                        'id_kecamatan' => $kec_id,
                        'id_kelurahan' => $desa_id,
                        'alamat' => $value->alamat_pemilik,
                        'full_name' => $value->nama_pemilik,
                        'npwp' => $value->npwp_pemilik,
                        'nomor_telephone' => $value->telepon1_pemilik,
                        'nomor_fax' => $value->faksimili1_pemilik,
                        'created_by' => $idUser,
                        'modified_by' => $idUser
                    );
                    $this->userProfile->insert($dataUser);

                    $dataSiup = array(
                        'nomor' => $tipe == 'nelayan_kecil' ? $nib : $siup,
                        'created_by' => $idUser,
                        'modified_by' => $idUser,
                        'catatan' => $value->catatan_lampiran_siup,
                        'file_pdf_siup' => $value->file_pdf_siup ? Download($value->file_pdf_siup, $this->siupPath, $siup) : '-',
                        'foto_pemilik' => $value->foto_pemilik ? cekDownload($value->foto_pemilik, $this->siupPath) : '-',
                        'jabatan_penanda_tangan' => 'jabatan penandatangan',
                        'nama_penanda_tangan' => 'nama penadatangan',
                        'nomor_nib' => $value->nomor_nib ? $value->nomor_nib : '-',
                        'owner' => $idUser,
                        'parameter_resiko' => 'no',
                        'pemilik_manfaat' => $value->nama_pemilik,
                        'penanggung_jawab' => $value->penanggung_jawab ? $value->penanggung_jawab : '-',
                        'ruang_lingkup_kegiatan' => $value->nama_jenis_kegiatan ? $value->nama_jenis_kegiatan : '',
                        'tanggal_berlaku_akhir' => $value->tanggal_berlaku ? $value->tanggal_berlaku : '-',
                        'tanggal_berlaku_awal' => $value->tanggal_terbit ? $value->tanggal_terbit : '-',
                        'tanggal_tanda_tangan' => $value->tanggal_tanda_tangan ? $value->tanggal_tanda_tangan : '',
                        'tanggal_terbit' => $value->tanggal_terbit ? $value->tanggal_terbit : '-',
                        'tipe_surat_izin' => $tipe == 'nelayan_kecil' ? NELAYAN_KECIL : NELAYAN_BESAR
                    );

                    $this->siup->insert($dataSiup);

                    foreach ($value->alokasi_siup as $key => $alokasi) {
                        # code...
                        $jenisKapal = cekJenisKapal($alokasi->jenis_kapal);
                        $alatTangkap = $tipe == 'nelayan_kecil' ? cekAlatTangkap($alokasi->nama_alat_tangkap) : cekAlatTangkap($alokasi->alat_tangkap);
                        // $ukuranKapal = cekUkuranKapal($alokasi->batas_atas, $alokasi->batas_bawah);

                        $data = array(
                            'nomor_siup' => $tipe == 'nelayan_kecil' ? $nib : $siup,
                            'id_alat_penangkap_ikan' => $alatTangkap,
                            'id_jenis_kapal' => $jenisKapal,
                            'batas_atas' => $alokasi->batas_atas,
                            'batas_bawah' => $alokasi->batas_bawah,
                            'jumlah_alokasi' => $alokasi->jumlah_alokasi,
                            'created_by' =>  $idUser,
                            'modified_by' => $idUser
                        );

                        $this->alokasi->insert($data);

                        if ($tipe == 'nelayan_kecil') {
                            if ($alokasi->kbli) {

                                $kbl = explode(',', $alokasi->kbli);

                                foreach ($kbl as $key => $kblis) {
                                    # code...
                                    $hasil =  explode('-', $kblis);
                                    # code...

                                    $kbliss =  cekKbli(trim($hasil[0], " "), $hasil[1]);
                                    if ($kbliss) {
                                        $dataKbli = array(
                                            'id_alat_penangkap_ikan' => $alatTangkap,
                                            'id_jenis_kapal' => $jenisKapal,
                                            'id_kbli' => $kbliss,
                                            'batas_atas' => $alokasi->batas_atas,
                                            'batas_bawah' => $alokasi->batas_bawah,
                                            'nomor_siup' =>  $nib,
                                            'created_by' =>  $idUser,
                                            'modified_by' => $idUser
                                        );
                                        $this->inkbli->insert($dataKbli);
                                    }
                                }
                            }
                        } else {
                            if ($alokasi->alokasi_kbli) {

                                $kbli = explode(',', $alokasi->alokasi_kbli);

                                foreach ($kbli as $key => $kblis) {
                                    # code...
                                    $hasil =  explode('-', $kblis);
                                    # code...

                                    $kbliss =  cekKbli(trim($hasil[0], " "), $hasil[1]);
                                    if ($kbliss) {
                                        $dataKbli = array(
                                            'id_alat_penangkap_ikan' => $alatTangkap,
                                            'id_jenis_kapal' => $jenisKapal,
                                            'id_kbli' => $kbliss,
                                            'batas_atas' => $alokasi->batas_atas,
                                            'batas_bawah' => $alokasi->batas_bawah,
                                            'nomor_siup' => $siup,
                                            'created_by' =>  $idUser,
                                            'modified_by' => $idUser
                                        );
                                        $this->inkbli->insert($dataKbli);
                                    }
                                }
                            }
                        }

                        if ($tipe == 'nelayan_kecil') {
                            if ($alokasi->dpi != "-" && $alokasi->dpi != "--" && $alokasi->dpi != null) {
                                $dpix = explode(',', $alokasi->dpi);

                                foreach ($dpix as $key => $dpiss) {

                                    $dpisss = cekDpi($dpiss);
                                    if ($dpisss) {
                                        $dataDpi = array(
                                            'id_daerah_penangkapan_ikan' => $dpisss,
                                            'id_alat_penangkap_ikan' => $alatTangkap,
                                            'id_jenis_kapal' => $jenisKapal,
                                            'batas_atas' => $alokasi->batas_atas,
                                            'batas_bawah' => $alokasi->batas_bawah,
                                            'nomor_siup' => $nib,
                                            'created_by' =>  $idUser,
                                            'modified_by' => $idUser
                                        );

                                        $this->inDpi->insert($dataDpi);
                                    }
                                }
                            }
                        } else {

                            if ($alokasi->alokasi_dpi != "-" && $alokasi->alokasi_dpi != "--" && $alokasi->alokasi_dpi != null) {

                                $dpix = explode(',', $alokasi->alokasi_dpi);

                                foreach ($dpix as $key => $dpiss) {

                                    $dpisss = cekDpi($dpiss);

                                    if ($dpisss) {
                                        $dataDpi = array(
                                            'id_daerah_penangkapan_ikan' => $dpisss,
                                            'id_alat_penangkap_ikan' => $alatTangkap,
                                            'id_jenis_kapal' => $jenisKapal,
                                            'batas_atas' => $alokasi->batas_atas,
                                            'batas_bawah' => $alokasi->batas_bawah,
                                            'nomor_siup' => $siup,
                                            'created_by' =>  $idUser,
                                            'modified_by' => $idUser
                                        );

                                        $this->inDpi->insert($dataDpi);
                                    }
                                }
                            }
                        }

                        if ($tipe == 'nelayan_kecil') {
                            if ($alokasi->pelabuhan_pangkalan != "-" && $alokasi->pelabuhan_pangkalan != "--" && $alokasi->pelabuhan_pangkalan != null) {
                                $pangkalan = explode(',', $alokasi->pelabuhan_pangkalan);
                                foreach ($pangkalan as $key => $pangkalans) {
                                    $pelabuhanPangkalan = cekPelabuhanPangkalan($pangkalans);
                                    if ($pelabuhanPangkalan) {
                                        $dataPangkalan = array(
                                            'id_alat_penangkap_ikan' => $alatTangkap,
                                            'id_jenis_kapal' => $jenisKapal,
                                            'batas_atas' => $alokasi->batas_atas,
                                            'batas_bawah' => $alokasi->batas_bawah,
                                            'id_pelabuhan' => $pelabuhanPangkalan,
                                            'nomor_siup' => $nib,
                                            'created_by' =>  $idUser,
                                            'modified_by' => $idUser
                                        );
                                        $this->inPangkalan->insert($dataPangkalan);
                                    }
                                }
                            }
                        } else {
                            if ($alokasi->alokasi_pelabuhan_pangkalan != "-" && $alokasi->alokasi_pelabuhan_pangkalan != "--" && $alokasi->alokasi_pelabuhan_pangkalan != null) {
                                $pangkalan = explode(',', $alokasi->alokasi_pelabuhan_pangkalan);
                                foreach ($pangkalan as $key => $pangkalans) {
                                    $pelabuhanPangkalan = cekPelabuhanPangkalan($pangkalans);
                                    if ($pelabuhanPangkalan) {
                                        $dataPangkalan = array(
                                            'id_alat_penangkap_ikan' => $alatTangkap,
                                            'id_jenis_kapal' => $jenisKapal,
                                            'batas_atas' => $alokasi->batas_atas,
                                            'batas_bawah' => $alokasi->batas_bawah,
                                            'id_pelabuhan' => $pelabuhanPangkalan,
                                            'nomor_siup' => $siup,
                                            'created_by' =>  $idUser,
                                            'modified_by' => $idUser
                                        );
                                        $this->inPangkalan->insert($dataPangkalan);
                                    }
                                }
                            }
                        }

                        if ($tipe == 'nelayan_kecil') {
                            if ($alokasi->pelabuhan_muat != "-" && $alokasi->pelabuhan_muat != "--" && $alokasi->pelabuhan_muat != null) {
                                $muat = explode(',', $alokasi->pelabuhan_muat);
                                foreach ($muat as $key => $muats) {
                                    # code...
                                    $pelabuhanMuat = cekAlokasiPelabuhanMuat($muats);

                                    if ($pelabuhanMuat) {
                                        $dataMuat = array(
                                            'id_alat_penangkap_ikan' => $alatTangkap,
                                            'id_jenis_kapal' => $jenisKapal,
                                            'batas_atas' => $alokasi->batas_atas,
                                            'batas_bawah' => $alokasi->batas_bawah,
                                            'id_pelabuhan' => $pelabuhanMuat,
                                            'nomor_siup' => $nib,
                                            'created_by' =>  $idUser,
                                            'modified_by' => $idUser
                                        );
                                        $this->inPelabuhanMuat->insert($dataMuat);
                                    }
                                }
                            }
                        } else {
                            if ($alokasi->alokasi_pelabuhan_muat != "-" && $alokasi->alokasi_pelabuhan_muat != "--" && $alokasi->alokasi_pelabuhan_muat != null) {
                                $muat = explode(',', $alokasi->alokasi_pelabuhan_muat);
                                foreach ($muat as $key => $muats) {
                                    # code...
                                    $pelabuhanMuat = cekAlokasiPelabuhanMuat($muats);

                                    if ($pelabuhanMuat) {
                                        $dataMuat = array(
                                            'id_alat_penangkap_ikan' => $alatTangkap,
                                            'id_jenis_kapal' => $jenisKapal,
                                            'batas_atas' => $alokasi->batas_atas,
                                            'batas_bawah' => $alokasi->batas_bawah,
                                            'id_pelabuhan' => $pelabuhanMuat,
                                            'nomor_siup' => $siup,
                                            'created_by' =>  $idUser,
                                            'modified_by' => $idUser
                                        );
                                        $this->inPelabuhanMuat->insert($dataMuat);
                                    }
                                }
                            }
                        }

                        if ($tipe == 'nelayan_kecil') {
                            if ($alokasi->pelabuhan_negara_tujuan != "-" && $alokasi->pelabuhan_negara_tujuan != "--" && $alokasi->pelabuhan_negara_tujuan != null) {
                                $tujuan = explode(',', $alokasi->pelabuhan_negara_tujuan);
                                foreach ($tujuan as $key => $tujuans) {
                                    # code...
                                    $pelabuhanTujuan = cekAlokasiPelabuhanTujuan($tujuans);
                                    if ($pelabuhanTujuan) {
                                        $dataTujuan = array(
                                            'id_alat_penangkap_ikan' => $alatTangkap,
                                            'id_jenis_kapal' => $jenisKapal,
                                            'batas_atas' => $alokasi->batas_atas,
                                            'batas_bawah' => $alokasi->batas_bawah,
                                            'id_pelabuhan' => $pelabuhanTujuan,
                                            'nomor_siup' => $nib,
                                            'created_by' =>  $idUser,
                                            'modified_by' => $idUser
                                        );
                                        $this->inPelabuhanTujuan->insert($dataTujuan);
                                    }
                                }
                            }
                        } else {
                            if ($alokasi->alokasi_negara_tujuan != "-" && $alokasi->alokasi_negara_tujuan != "--" && $alokasi->alokasi_negara_tujuan != null) {
                                $tujuan = explode(',', $alokasi->alokasi_negara_tujuan);
                                foreach ($tujuan as $key => $tujuans) {
                                    # code...
                                    $pelabuhanTujuan = cekAlokasiPelabuhanTujuan($tujuans);
                                    if ($pelabuhanTujuan) {
                                        $dataTujuan = array(
                                            'id_alat_penangkap_ikan' => $alatTangkap,
                                            'id_jenis_kapal' => $jenisKapal,
                                            'batas_atas' => $alokasi->batas_atas,
                                            'batas_bawah' => $alokasi->batas_bawah,
                                            'id_pelabuhan' => $pelabuhanTujuan,
                                            'nomor_siup' => $siup,
                                            'created_by' =>  $idUser,
                                            'modified_by' => $idUser
                                        );
                                        $this->inPelabuhanTujuan->insert($dataTujuan);
                                    }
                                }
                            }
                        }
                    }
                }

                $this->db->transCommit();

                if ($this->config->requireActivation !== null) {
                    $activator = service('activator');
                    $sent      = $activator->send($user);

                    // Success!
                    $response = [
                        'code' => 200,
                        'error' => true,
                        'message' => lang('Auth.activationSuccess'),
                        'data' => []
                    ];
                    return $this->respondCreated($response);
                }
            } catch (\Throwable $th) {
                $this->db->transRollback();
                //throw $th;
                $response = [
                    'code' => 400,
                    'error' => false,
                    'message' => $th->getMessage(),
                    'data' => null
                ];
                return $this->response->setJSON($response);
            }
        } else {
            $response = [
                'code' => 403,
                'error' => true,
                'message' => 'Recaptcha Not Valid',
                'data' => []
            ];
            return $this->response->setJSON($response);
        }
    }

    public function activateAccount()
    {
        $users = model(UserModel::class);

        // First things first - log the activation attempt.
        $users->logActivationAttempt(
            $this->request->getGet('token'),
            $this->request->getIPAddress(),
            (string) $this->request->getUserAgent()
        );

        $throttler = service('throttler');

        if ($throttler->check(md5($this->request->getIPAddress()), 2, MINUTE) === false) {
            $response = [
                'status' => 429,
                'error' => true,
                'message' => lang('Auth.tooManyRequests'),
                'data' => [
                    'request' => $throttler->getTokentime()
                ]
            ];
            return $this->respond($response, 429);
            // return service('response')->setStatusCode(429)->setBody(lang('Auth.tooManyRequests', [$throttler->getTokentime()]));
        }

        $user = $users->where('activate_hash', $this->request->getGet('token'))
            ->where('active', 0)
            ->first();

        if (null === $user) {
            $response = [
                'status' => 400,
                'error' => true,
                'message' => lang('Auth.activationNoUser'),
                'data' => []
            ];
            return view('Auth/emails/activated', $response);
        }

        $user->activate();

        $users->save($user);

        $response = [
            'status' => 200,
            'error' => true,
            'message' => lang('Auth.registerSuccess'),
            'data' => []
        ];
        // return $this->respondCreated($response);

        return view('Auth/emails/activated');
    }

    public function attemptForgot()
    {
        $rules = [
            'email' => [
                'label' => lang('Auth.emailAddress'),
                'rules' => 'required|valid_email',
            ],
        ];

        if (!$this->validate($rules)) {
            $response = [
                'status' => 400,
                'error' => true,
                'message' => $this->validator->getErrors(),
                'data' => []
            ];
            return $this->respond($response, 400);
        }

        $users = model(UserModel::class);

        $user = $users->where('email', $this->request->getPost('email'))->first();

        if (null === $user) {
            $response = [
                'status' => 400,
                'error' => true,
                'message' => lang('Auth.forgotNoUser'),
                'data' => []
            ];
            return $this->respond($response);
        }

        // Save the reset hash /
        $user->generateResetHash();
        $users->save($user);

        $resetter = service('resetter');
        $sent     = $resetter->send($user);

        if (!$sent) {
            $response = [
                'status' => 400,
                'error' => true,
                'message' => $resetter->error() ?? lang('Auth.unknownError'),
                'data' => []
            ];
            return $this->respond($response, 400);
        }

        $response = [
            'status' => 200,
            'error' => true,
            'message' => lang('Auth.forgotEmailSent'),
            'data' => []
        ];
        return $this->respondCreated($response);
    }

    public function resetPassword()
    {
        if ($this->config->activeResetter === null) {
            return redirect()
                ->route("login")
                ->with("error", lang("Auth.forgotDisabled"));
        }

        $token = $this->request->getGet("token");

        return view($this->config->views["reset"], [
            "config" => $this->config,
            "token" => $token,
        ]);
    }

    public function attemptReset()
    {
        if ($this->config->activeResetter === null) {
            return redirect()
                ->route("login")
                ->with("error", lang("Auth.forgotDisabled"));
        }

        $users = model(UserModel::class);

        // First things first - log the reset attempt.
        $users->logResetAttempt(
            $this->request->getPost("email"),
            $this->request->getPost("token"),
            $this->request->getIPAddress(),
            (string) $this->request->getUserAgent()
        );

        $rules = [
            "token" => "required",
            "email" => "required|valid_email",
            "password" => "required",
            // 'pass_confirm' => 'required|matches[password]',
            "pass_confirm" => [
                "rules" => "required|matches[password]",
                "label" => "Password Confirmation",
            ],
        ];

        if (!$this->validate($rules)) {
            $response = [
                'code' => 400,
                'error' => true,
                'message' => $this->validator->getErrors(),
                'data' => []
            ];
            return $this->respond($response, 400);
            // return redirect()
            //     ->back()
            //     ->withInput()
            //     ->with("errors", $this->validator->getErrors());
        }

        $user = $users
            ->where("email", $this->request->getPost("email"))
            ->where("reset_hash", $this->request->getPost("token"))
            ->first();

        if (is_null($user)) {

            $response = [
                'code' => 400,
                'error' => true,
                'message' => lang("Auth.forgotNoUser"),
                'data' => []
            ];
            return $this->respond($response, 400);

            // return redirect()
            //     ->back()
            //     ->with("error", lang("Auth.forgotNoUser"));
        }

        // Reset token still valid?
        if (
            !empty($user->reset_expires) &&
            time() > $user->reset_expires->getTimestamp()
        ) {
            $response = [
                'code' => 400,
                'error' => true,
                'message' => lang("Auth.resetTokenExpired"),
                'data' => []
            ];
            return $this->respond($response, 400);

            // return redirect()
            //     ->back()
            //     ->withInput()
            //     ->with("error", lang("Auth.resetTokenExpired"));
        }

        // Success! Save the new password, and cleanup the reset hash.
        $user->password = $this->request->getPost("password");
        $user->reset_hash = null;
        $user->reset_at = date("Y-m-d H:i:s");
        $user->reset_expires = null;
        $user->force_pass_reset = false;
        $users->save($user);

        $response = [
            'code' => 200,
            'error' => true,
            'message' =>  lang("Auth.resetSuccess"),
        ];
        return $this->respond($response, 400);

        // return redirect()
        //     ->route("login")
        //     ->with("message", lang("Auth.resetSuccess"));
    }


    public function cekSiup()
    {
        $siup = $this->request->getPost('siup');
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $res = $client->request('GET', 'https://perizinan.kkp.go.id/silat/eservices/webservice/query?target=kapi_siup_aktif&pass=pdk@0816&q1=' . $siup . '&ver=2', [
            'timeout' => 10,
            'connect_timeout' => 10
        ]);
        // $res = $client->request('GET', 'https://perizinan.kkp.go.id/silat/eservices/webservice/query?target=vw_webservice_izin_usaha&pass=2019pdk&&q1='.$siup, [

        // ]);
        $resp = json_decode($res->getBody());

        if ($resp->status == 'ok') {
            $response = json_decode($res->getBody());
        } else {
            $response = [
                'code' => 400,
                'error' => true,
                'message' => "Failed ",
                'data' => []

            ];
        }
        return $this->respond($response);
    }

    public function cekNib()
    {
        $nib = $this->request->getPost('nib');
        $pemilik = $this->request->getPost('pemilik');

        $headers = [
            "accept: /",
            "accept-language: en-US,en;q=0.8",
            "content-type: application/json",
        ];
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $res = $client->request('GET', 'https://perizinan.kkp.go.id/silat/eservices/webservice/query?target=kapi_simkada_siup_aktif&pass=pdk@0816&q1=' . $pemilik . '&q2=' . $nib . '&ver=2', [
            'headers' => $headers,
            'timeout' => 10,
            'connect_timeout' => 10
        ]);

        $resp =  json_decode($res->getBody()->getContents());

        if ($resp->status == 'ok') {
            $status = 200;
            $response = $resp;
        } else {
            $response = [
                'status' => 400,
                'error' => true,
                'message' => "Failed ",
                'data' => []
            ];
            $status = 400;
        }
        return $this->respond($response, $status);
        // echo $res->getBody()->getContents();

    }

    public function cekNama()
    {

        $apiURL = "https://kapal.dephub.go.id/ditkapel_service/data_kapal/data_kapal.php";
        $postInput = [
            'nama_kapal' => $this->request->getVar('nama_kapal'),
            'nama_pemilik' => $this->request->getVar('nama_pemilik'),
        ];
        $headers = [
            "accept: */*",
            "accept-language: en-US,en;q=0.8",
        ];

        $client = new \GuzzleHttp\Client(['verify' => false]);
        $response = $client->request('POST', $apiURL, [
            'form_params' => $postInput, 'headers' => $headers, 'timeout' => 10,
            'connect_timeout' => 10
        ]);
        $res = json_decode($response->getBody());
        // var_dump(intval(count($res)) );
        // die;
        if (count($res) > 1) {
            $status = 400;
            $response = [
                'code' => 400,
                'error' => true,
                'message' => "Please check full name of the ship ",
                'data' => []

            ];
        } else {
            $status = 200;
            $response = [
                'code' => 200,
                'error' => false,
                'message' => "Success ",
                'data' => $res

            ];
        }
        return $this->respond($response, $status);
    }

    public function getHubla()
    {

        $client = new Client();
        $res = $client->request('POST', 'https://kapal.kkp.go.id/sikapi/pendok/permohonan_new/cek_no_tp', [
            'form_params' => [
                'no_tp' => $this->request->getVar('no_tp'),
            ],
            'timeout' => 10,
            'connect_timeout' => 10
        ]);
        // echo $res->getStatusCode();
        return $this->response->setJSON($res->getBody()->getContents());
    }

    private function getKey()
    {
        return "bijiMataLu";
    }

    public function patchProvinsi()
    {
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $prov = new ProvincesModel();
        $res = $client->request('GET', 'https://ibnux.github.io/data-indonesia/provinsi.json', []);
        $response = json_decode($res->getBody()->getContents());
        foreach ($response as $key => $value) {
            # code... 
            // $data = [
            //     'id' => $value->id,
            //     'nama' => $value->nama,
            //     'created_by' =>1,
            //     'modified_by' =>1
            // ];
            // $prov->insert($data);
            $kabsss = new KabupatenModel();
            # code...
            $kab = $client->request('GET', 'https://ibnux.github.io/data-indonesia/kabupaten/' . $value->id . '.json', []);
            $kabs = json_decode($kab->getBody()->getContents());
            foreach ($kabs as $key => $kabss) {
                $idKab = str_split($kabss->id, 2);
                // var_dump(str_split($kabss->id, 2));
                // die;
                // 
                // $data = [
                //     'id_provinsi' => $idKab[0],
                //     'id_kabupaten' => $idKab[1],
                //     'nama' => $kabss->nama,
                //     'created_by' => 1,
                //     'modified_by' => 1
                // ];
                // $kabsss->insert($data);
                $insertkec = new KecamatanModel();
                $kec = $client->request('GET', 'https://ibnux.github.io/data-indonesia/kecamatan/' . $kabss->id . '.json', []);
                $kecs = json_decode($kec->getBody()->getContents());
                foreach ($kecs as $key => $kecss) {
                    $idKec = str_split($kecss->id, 2);

                    // $data = [
                    //     'id_provinsi' => $idKec[0],
                    //     'id_kabupaten' => $idKec[1],
                    //     'id_kecamatan' => $idKec[2],
                    //     'nama' => $kecss->nama,
                    //     'created_by' => 1,
                    //     'modified_by' => 1
                    // ];
                    // $insertkec->insert($data);

                    $insertkel = new KelurahanModel();
                    $kel = $client->request('GET', 'https://ibnux.github.io/data-indonesia/kelurahan/' . $kecss->id . '.json', []);
                    $kels = json_decode($kel->getBody()->getContents());
                    foreach ($kels as $key => $kelss) {
                        $idKel = str_split($kelss->id, 6);
                        $idKels = str_split($kelss->id, 2);
                        // var_dump($idKel);
                        // die;
                        $data = [
                            'id_provinsi' => $idKels[0],
                            'id_kabupaten' => $idKels[1],
                            'id_kecamatan' => $idKels[2],
                            'id_kelurahan' => $idKel[1],
                            'nama' => $kelss->nama,
                            'created_by' => 1,
                            'modified_by' => 1
                        ];
                        $insertkel->insert($data);
                    }
                }
            }
        }
    }
}
