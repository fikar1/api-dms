<?php

namespace App\Entities;

use CodeIgniter\Commands\Server\Serve;
use Config\Services;

class Collection
{
    protected $request;
    public function __construct()
    {
        
    }

    public static function datatable(array $data, int $recordsTotal, int $recordsFiltered)
    {
        $request =  service('request');
        return [
            // 'draw'            => intval($request->getGet('draws')),
            'recordsTotal'    => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data'            => $data,
        ];
    }
}
