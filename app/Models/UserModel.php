<?php

namespace App\Models;

use CodeIgniter\Model;
use Faker\Generator;
use Myth\Auth\Authorization\GroupModel;
use Myth\Auth\Entities\User;

/**
 * @method User|null first()
 */
class UserModel extends Model
{
    const ORDERABLE = [
        1 => 'username',
        2 => 'email',
        4 => 'created_at',
    ];

    protected $table          = 'users';
    protected $primaryKey     = 'id';
    protected $order = ['id' => 'DESC'];
    protected $returnType     = 'App\Entities\User';
    protected $useSoftDeletes = true;
    protected $allowedFields  = [
        'email', 'username', 'password_hash', 'reset_hash', 'reset_at', 'reset_expires', 'activate_hash',
        'status', 'status_message', 'active', 'force_pass_reset', 'permissions', 'deleted_at', 'avatar', 'uri_path', 'nomor_hp', 'connectionID', 'sessionID'
    ];
    protected $useTimestamps   = true;
    protected $validationRules = [
        'email'         => 'required|valid_email|is_unique[users.email,id,{id}]',
        'username'      => 'required|alpha_numeric_punct|min_length[3]|max_length[30]|is_unique[users.username,id,{id}]',
        'password_hash' => 'required',
    ];
    protected $validationMessages = [];
    protected $skipValidation     = false;
    protected $afterInsert        = ['addToGroup'];

    /**
     * The Profile.
     * Set internally by user.
     *
     * @var int|null
     */
    protected $assignProfile;
    /**
     * The id of a group to assign.
     * Set internally by withGroup.
     *
     * @var int|null
     */
    protected $assignGroup;

    /**
     * Logs a password reset attempt for posterity sake.
     */
    public function logResetAttempt(string $email, ?string $token = null, ?string $ipAddress = null, ?string $userAgent = null)
    {
        $this->db->table('auth_reset_attempts')->insert([
            'email'      => $email,
            'ip_address' => $ipAddress,
            'user_agent' => $userAgent,
            'token'      => $token,
            'created_at' => date('Y-m-d H:i:s'),
        ]);
    }

    /**
     * Logs an activation attempt for posterity sake.
     */
    public function logActivationAttempt(?string $token = null, ?string $ipAddress = null, ?string $userAgent = null)
    {
        $this->db->table('auth_activation_attempts')->insert([
            'ip_address' => $ipAddress,
            'user_agent' => $userAgent,
            'token'      => $token,
            'created_at' => date('Y-m-d H:i:s'),
        ]);
    }

    /**
     * Sets the group to assign any users created.
     *
     * @return $this
     */
    public function withGroup(string $groupName)
    {
        $group = $this->db->table('auth_groups')->where('name', $groupName)->get()->getFirstRow();

        $this->assignGroup = $group->id;

        return $this;
    }

    /**
     * Clears the group to assign to newly created users.
     *
     * @return $this
     */
    public function clearGroup()
    {
        $this->assignGroup = null;

        return $this;
    }

    /**
     * If a default role is assigned in Config\Auth, will
     * add this user to that group. Will do nothing
     * if the group cannot be found.
     *
     * @param mixed $data
     *
     * @return mixed
     */
    protected function addToGroup($data)
    {
        if (is_numeric($this->assignGroup)) {
            $groupModel = model(GroupModel::class);
            $groupModel->addUserToGroup($data['id'], $this->assignGroup);
        }

        return $data;
    }
    /**
     * Faked data for Fabricator.
     */
    public function fake(Generator &$faker): User
    {
        return new User([
            'email'    => $faker->email,
            'username' => $faker->userName,
            'password' => bin2hex(random_bytes(16)),
        ]);
    }

    public function getResource(string $search = '')
    {
        $builder = $this->builder()
            ->select('users.*,user_profile.alamat,user_profile.idprovinsi as id_provinsi,user_profile.id_kabupaten,user_profile.id_kecamatan,user_profile.id_kelurahan,auth_groups.name as group, user_profile.full_name,auth_groups.id as id_role')
            ->join('user_profile', 'user_profile.user_id=users.id');
        $builder->join('auth_groups_users', 'auth_groups_users.user_id=users.id', 'left');
        $builder->join('auth_groups', 'auth_groups.id=auth_groups_users.group_id', 'left');
        $condition = empty($search)
            ? $builder
            : $builder->groupStart()
            ->like('username', $search)
            ->orLike('email', $search)
            ->groupEnd();
        return $condition->where('deleted_at', null);
    }

    public function withProfile($id)
    {
        return $this->builder()
            ->select('users.id, username,user_profile.full_name, user_profile.alamat, email,avatar, nomor_hp,users.created_at,users.updated_at,users.active')
            ->join('user_profile', 'user_profile.user_id=users.id', 'left')

            ->where('users.id', $id)
            ->get()
            ->getFirstRow();
    }

    public function getUserBySession($sessionID)
    {
        return $this->builder()->where('users.sessionID', $sessionID)
            ->get()
            ->getFirstRow();
    }

    public function userData($id)
    {
        return $this->builder()->where('id', $id)->get()->getFirstRow();
    }

    public function getConnectionID($id)
    {
        return $this->builder()->where('connectionID', $id)->get()->getFirstRow();
    }

    public function countConnectionID($id)
    {
        return $this->builder()->where('connectionID', $id)->countAllResults();
    }
}
