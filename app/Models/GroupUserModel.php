<?php

namespace App\Models;

use CodeIgniter\Model;

class GroupUserModel extends Model
{

    protected $table            = 'auth_groups';
    protected $allowedFields = [
        'description',
        'name',
    ];

    //get all data permission
    public function getResource(string $search = '')
    {
        $builder = $this->builder()
            ->select('id,name,description');

        $condition = empty($search)
            ? $builder
            : $builder->groupStart()
            ->like('name', $search)
            ->orLike('description', $search)
            ->groupEnd();

        return $condition;
    }
}
