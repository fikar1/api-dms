<?php

namespace App\Libraries;

class Captcha
{

    public function store($captcha)
    {

        $credential = array(
            'secret' => RECAPTCHAV2_SECRET,
            'response' => $captcha
        );
        $verify = curl_init();
        curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($verify, CURLOPT_POST, true);
        curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($credential));
        curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($verify);
        $status = json_decode($response, true);

        if ($status['success']) {
            return true;
        } else {
            return false;
        }
    }
}
