<?php

namespace App\Models;

use CodeIgniter\Database\BaseResult;
use CodeIgniter\Database\Query;
use CodeIgniter\Model;
use Faker\Generator;
use Myth\Auth\Entities\Permission;
use PhpParser\Node\Expr\Cast\Array_;

class PermissionModel extends Model
{
    protected $table         = 'auth_permissions';
    protected $returnType    = Permission::class;
    protected $allowedFields = [
        'name',
        'description',
    ];
    protected $validationRules = [
        'name'        => 'required|max_length[255]|is_unique[auth_permissions.name,name,{name}]',
        'description' => 'max_length[255]',
    ];
    //get all data permission
    public function getResource(string $search = '')
    {
        $builder = $this->builder()
            ->select('id,name,description');

        $condition = empty($search)
            ? $builder
            : $builder->groupStart()
            ->like('name', $search)
            ->orLike('description', $search)
            ->groupEnd();

        return $condition;
    }
    /**
     * Checks to see if a user, or one of their groups,
     * has a specific permission.
     */
    public function doesUserHavePermission(int $userId, int $permissionId): bool
    {
        return array_key_exists($permissionId, $this->getPermissionsForUser($userId));
    }

    /**
     * Adds a single permission to a single user.
     *
     * @return BaseResult|false|Query
     */
    public function addPermissionToUser(int $permissionId, int $userId)
    {
        cache()->delete("{$userId}_permissions");

        return $this->db->table('auth_users_permissions')->insert([
            'user_id'       => $userId,
            'permission_id' => $permissionId,
        ]);
    }



    /**
     * Removes a permission from a user.
     *
     * @return bool
     */
    public function removePermissionFromUser(int $permissionId, int $userId)
    {
        cache()->delete("{$userId}_permissions");

        return $this->db->table('auth_users_permissions')->where([
            'user_id'       => $userId,
            'permission_id' => $permissionId,
        ])->delete();
    }

    /**
     * Gets all permissions for a user in a way that can be
     * easily used to check against:
     *
     * @return array<int, string> An array in format permissionId => permissionName
     */
    public function getPermissionsForUser(int $userId): array
    {
        if (null === $found = cache("{$userId}_permissions")) {
            $fromUser = $this->db->table('auth_users_permissions')
                ->select('id, auth_permissions.name')
                ->join('auth_permissions', 'auth_permissions.id = permission_id', 'inner')
                ->where('user_id', $userId)
                ->get()
                ->getResultObject();
            $fromGroup = $this->db->table('auth_groups_users')
                ->select('auth_permissions.id, auth_permissions.name')
                ->join('auth_groups_permissions', 'auth_groups_permissions.group_id = auth_groups_users.group_id', 'inner')
                ->join('auth_permissions', 'auth_permissions.id = auth_groups_permissions.permission_id', 'inner')
                ->where('user_id', $userId)
                ->get()
                ->getResultObject();

            $combined = array_merge($fromUser, $fromGroup);

            $found = [];

            foreach ($combined as $row) {
                $found[$row->id] = strtolower($row->name);
            }

            cache()->save("{$userId}_permissions", $found, 300);
        }

        return $found;
    }

    public function getPermissionsForUsers(int $userId): array
    {
        // if (null === $found = cache("{$userId}_permissions")) {
        $fromUser = $this->db->table('auth_users_permissions')
            ->select('id, auth_permissions.name')
            ->join('auth_permissions', 'auth_permissions.id = permission_id', 'inner')
            ->where('user_id', $userId)
            ->get()
            ->getResultObject();
        $fromGroup = $this->db->table('auth_groups_users')
            ->select('auth_permissions.id, auth_permissions.name')
            ->join('auth_groups_permissions', 'auth_groups_permissions.group_id = auth_groups_users.group_id', 'inner')
            ->join('auth_permissions', 'auth_permissions.id = auth_groups_permissions.permission_id', 'inner')
            ->where('user_id', $userId)
            ->get()
            ->getResultObject();

        $combined = array_merge($fromUser, $fromGroup);

        $found = [];

        foreach ($combined as $row) {
            $found[] = array(
                'id' => $row->id,
                'name' => strtolower($row->name)
            );
        }

        //     cache()->save("{$userId}_permissions", $found, 300);
        // }

        return $found;
    }

    /**
     * Gets all permissions for a user in a way that can be
     * easily used to check against:
     *
     * @return array<int, string> An array in format groupid => permissionName
     */

    public function getPermissionsGroup()
    {
        $fromGroup = $this->db->table('auth_groups')
            ->select('id, auth_groups.name')
            ->get()
            ->getResultObject();

        $found = [];

        foreach ($fromGroup as $row) {

            // $frompermission = $this->db->table('auth_groups')
            //         ->select('auth_permissions.id, auth_permissions.name')
            //         ->join('auth_groups_permissions', 'auth_groups_permissions.group_id = auth_groups.id', 'inner')
            //         ->join('auth_permissions', 'auth_permissions.id = auth_groups_permissions.permission_id', 'inner')
            //         ->where('group_id', $row->id)
            //         ->get()
            //         ->getResultObject();
            $query = $this->db->query("SELECT p.id, a.group_id ,p.name ,
                CASE
                    WHEN a.group_id  is null THEN 0
                    ELSE 1
                END as chek FROM   auth_permissions p left OUTER JOIN 
                auth_groups_permissions a on a.permission_id=p.id and a.group_id =$row->id");
            $frompermission  =  $query->getResultArray();
            $found[] = [
                'id' => $row->id,
                'name' => strtolower($row->name),
                'permissions' => $frompermission
            ];
        }

        return $found;
    }

    public function getPermission()
    {

        $permission = $this->db->table('auth_permissions')->get()
            ->getResultObject();
        return $permission;
    }

    /**
     * Faked data for Fabricator.
     *
     * @return array|Permission See PermissionFaker
     */
    public function fake(Generator &$faker)
    {
        return new Permission([
            'name'        => $faker->word,
            'description' => $faker->sentence,
        ]);
    }
}
