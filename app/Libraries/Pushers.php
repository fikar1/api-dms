<?php

namespace App\Libraries;

use Pusher\Pusher;

class Pushers {
    private $push;

    function __construct()
    {
        $options = array(
            'cluster' => 'ap1',
            'useTLS' => true
          );
          $this->push = new Pusher(
            'a87b2f42b01a231b2879',
            '820207cf111cfe45f6d5',
            '1513006',
            $options
          );
    }

    public function send($data = []){

      $this->push->trigger('my-channel', 'my-event', $data);
    }
}