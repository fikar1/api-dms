<?php

namespace Config;

use CodeIgniter\Config\BaseConfig;
use CodeIgniter\Validation\CreditCardRules;
use CodeIgniter\Validation\FileRules;
use CodeIgniter\Validation\FormatRules;
use CodeIgniter\Validation\Rules;

class Validation extends BaseConfig
{
    // --------------------------------------------------------------------
    // Setup
    // --------------------------------------------------------------------

    /**
     * Stores the classes that contain the
     * rules that are available.
     *
     * @var string[]
     */
    public $ruleSets = [
        Rules::class,
        FormatRules::class,
        FileRules::class,
        CreditCardRules::class,
    ];

    /**
     * Specifies the views that are used to display the
     * errors.
     *
     * @var array<string, string>
     */
    public $templates = [
        'list'   => 'CodeIgniter\Validation\Views\list',
        'single' => 'CodeIgniter\Validation\Views\single',
        Rules::class,
        FormatRules::class,
        FileRules::class,
        CreditCardRules::class,
        \Myth\Auth\Authentication\Passwords\ValidationRules::class,
        PasswordStrength::class
    ];

    // --------------------------------------------------------------------
    // Rules
    // --------------------------------------------------------------------

    public $users = [
        'username' => [
            'label'  => 'Username',
            'rules'  => 'required|is_unique[users.username]',
            'errors' => [
                'required' => 'All accounts must have {field} provided',
            ],
        ],
        'password'     => 'required',
        'pass_confirm' => 'required|matches[password]',
        'email'        => 'required|valid_email',
    ];

    public $users_add = [
        'username' => [
            'label'  => 'Username',
            'rules'  => 'required|is_unique[users.username]',
            'errors' => [
                'required' => 'All accounts must have {field} provided',
            ],
        ],
        'email' => [
            'label'  => 'Email',
            'rules'  => 'required|is_unique[users.email]',
            'errors' => [
                'required' => 'Please check the Email field. It does not appear to be valid.',
            ],
        ],
        'nomor_hp' => [
            'label'  => 'Nomor Hp wajib',
            'rules'  => 'required|is_unique[users.nomor_hp]',
            'errors' => [
                'required' => 'Please check the Nomor HP field. It does not appear to be valid.',
            ],
        ],
    ];
    public $users_update = [
        'username' => [
            'label'  => 'Username',
            'rules'  => 'required|is_unique[users.username]',
            'errors' => [
                'required' => 'All accounts must have {field} provided',
            ],
        ],
        'email' => [
            'label'  => 'Email',
            'rules'  => 'required|is_unique[users.email]',
            'errors' => [
                'required' => 'Please check the Email field. It does not appear to be valid.',
            ],
        ],
    ];
}
