<?php

use App\Entities\User;
use App\Models\AlatTangkapModel;
use App\Models\DaerahPenangkapanIkanModel;
use App\Models\JenisKapalModel;
use App\Models\KabupatenModel;
use App\Models\KbliModel;
use App\Models\KecamatanModel;
use App\Models\KelurahanModel;
use App\Models\PelabuhanModel;
use App\Models\PelabuhanNegaraTujuanModel;
use App\Models\PelabuhanPangkalanModel;
use App\Models\PengajuanShiftModel;
use App\Models\PersetujuanNamaKapalModel;
use App\Models\ProvincesModel;
use App\Models\UkuranKapalModel;
use App\Models\UserModel;
use CodeIgniter\Files\File;
use GuzzleHttp\Client;
use Pusher\Pusher;

function loginUser(User $user)
{
    $request = \Config\Services::request();
    $request->user = $user;
}

function isLoggedIn()
{
    $request = \Config\Services::request();
    if ($request->user) {
        return TRUE;
    }
    return FALSE;
}

function getUsername()
{
    $request = \Config\Services::request();
    if (isLoggedIn()) {
        return $request->user->name;
    }
    return null;
}


function getUserid()
{
    $request = \Config\Services::request();
    if (isLoggedIn()) {
        return $request->user->id;
    }
    return null;
}

function getUser()
{
    $userModel = new UserModel();
    $request = \Config\Services::request();
    if (isLoggedIn()) {
        return  $userModel->find($request->user->id);
    }
    return null;
}

function getUserRoles()
{
    if (!isLoggedIn()) {
        return [];
    }
    $request = \Config\Services::request();
    return $request->user->roles;
}

function hasRole($role_name)
{
    $request = \Config\Services::request();
    if (!isLoggedIn()) {
        return FALSE;
    }
    foreach ($request->user->roles as $role) {
        if (strtolower($role) == strtolower($role_name)) {
            return TRUE;
        }
    }
    return FALSE;
}

function hasPermission($permission_name)
{
    $request = \Config\Services::request();
    if (!isLoggedIn()) {
        return FALSE;
    }
    foreach ($request->user->permissions as $permission) {
        if (strcasecmp($permission, $permission_name) == 0) {
            return TRUE;
        }
    }
    return FALSE;
}

function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

if (!function_exists('db_get_all_data')) {
    function db_get_all_data($table_name = null, $where = false, $sort = false)
    {
        // $ci =& get_instance();
        $db      = \Config\Database::connect();
        $query = $db->table($table_name);
        if ($where) {
            $query->getWhere($where);
        }
        if ($sort) {
            $query->orderBy($sort, 'ASC');
        }


        return $query->get()->getResult();
    }
}

if (!function_exists('uploadImage')) {
    function uploadImage($uploadedPath, $subdir = "")
    {
        $imageInfo = [];
        try {

            $path = ROOTPATH . "uploads/" . $uploadedPath;
            $targetDir = $subdir == "" ? WRITEPATH . "images" : WRITEPATH . "images/" . $subdir;
            $file = new \CodeIgniter\Files\File($path);
            $image = \Config\Services::image();
            $imageConfig = new \Config\Images();

            $thumbnailSize = $imageConfig->thumnailSize;
            $mediumSize = $imageConfig->mediumSize;

            $file = $file->move($targetDir);
            $path = $file->getPathname();

            $imageInfo['name'] = $file->getFilename();
            $imageInfo['file_path'] = $file->getPath();
            $imageInfo['uri_path'] = $imageConfig->IMG_URL_PATH;

            $destination = File::getDestination($path);

            // echo $destination;
            $image
                ->withFile($file->getPathname())
                ->resize($thumbnailSize['width'], $thumbnailSize['height'], true)
                ->save($destination);

            $file =  new File($destination);
            $imageInfo['thumbnail'] = $file->getFilename();
            $destination = File::getDestination($path);
            $image
                ->withFile($file->getPathname())
                ->resize($mediumSize['width'], $mediumSize['height'], true)
                ->save($destination);
            $file =  new File($destination);
            $imageInfo['medium'] = $file->getFilename();
            return $imageInfo;
        } catch (Exception $exc) {
            log_message('error', $exc->getMessage());
            return FALSE;
        }
    }
}


if (!function_exists('uploadImages')) {
    function uploadImages($uploadedPath, $subdir = "")
    {
        $path = WRITEPATH . "uploads/" . $uploadedPath;
        $targetDir = $subdir == "" ? ROOTPATH . "public/images" : ROOTPATH . "public/images/" . $subdir;

        if (!is_dir($targetDir)) {
            mkdir($targetDir, 0777, TRUE);
        }

        if (!file_exists($targetDir)) {
            mkdir($targetDir);
        }
        $file = new \CodeIgniter\Files\File($path);
        $image = \Config\Services::image();
        $imageConfig = new \Config\Images();

        $mediumSize = $imageConfig->mediumSize;

        $file = $file->move($targetDir);
        $path = $file->getPathname();

        if ($subdir) {
            $dir = '/' . $subdir;
        }
        $imageInfo['file_path'] = $file->getPath();
        $imageInfo['uri_path'] = $imageConfig->IMG_URL_PATH . $dir;

        $image
            ->withFile($file->getPathname())
            ->resize($mediumSize['width'], $mediumSize['height'], true)
            ->save($path);

        $file =  new File($path);
        $imageInfo['medium'] = $file->getFilename();
        return $imageInfo;
    }
}


if (!function_exists('checkProvinsi')) {
    function checkProvinsi($nama)
    {
        $query = new ProvincesModel();
        $prov = $query->where('nama', $nama)->get()->getRow();

        if (!empty($prov->id)) {
            return $prov->id;
        } else {
            $rows = $query->selectMax('id')->get()->getRow();
            $data = [
                "id" => intval($rows->id) + 1,
                "nama" => $nama,
                "created_by" => 1,
                "modified_by" => 1,
            ];
            $query->insert($data);
            return intval($rows->id) + 1;
        }
    }
}

if (!function_exists('checkKabupaten')) {
    function checkKabupaten($nama, $id_provinsi)
    {
        $query = new KabupatenModel();
        $kab = $query->where('nama', $nama)->get()->getRow();

        if (!empty($kab->id_kabupaten)) {
            return $kab->id_kabupaten;
        } else {
            $kabs  = $query->selectMax('id_kabupaten', 'id')->where('id_provinsi', $id_provinsi)->get()->getRow();

            $data = [
                "id_kabupaten" => intval($kabs->id) + 1,
                "id_provinsi" => $id_provinsi,
                "nama" => $nama ? $nama : 'not found',
                "created_by" => 1,
                "modified_by" => 1
            ];
            $query->insert($data);
            return intval($kabs->id) + 1;
        }
    }
}

if (!function_exists('checkKecamatan')) {
    function checkKecamatan($nama, $id_provinsi, $id_kabupaten)
    {
        $query = new KecamatanModel();
        $kecam = $query->where('nama', $nama)->get()->getRow();

        if (!empty($kecam->id_kecamatan)) {
            return $kecam->id_kecamatan;
        } else {
            $kec  = $query->selectMax('id_kecamatan', 'id')->where(['id_kabupaten' => $id_kabupaten, 'id_provinsi' => $id_provinsi])->get()->getRow();

            $data = [
                "id_kecamatan" => intval($kec->id) + 1,
                "id_provinsi" => $id_provinsi,
                "id_kabupaten" => $id_kabupaten,
                "nama" => $nama ? $nama : 'not found',
                "created_by" => 1,
                "modified_by" => 1
            ];

            $query->insert($data);

            return intval($kec->id) + 1;
        }
    }
}

if (!function_exists('checkKelurahan')) {
    function checkKelurahan($nama, $id_provinsi, $id_kabupaten, $id_kecamatan)
    {
        $query = new KelurahanModel();
        $kelu = $query->where('nama', $nama)->get()->getRow();
        if (!empty($kelu->id_kelurahan)) {
            return $kelu->id_kelurahan;
        } else {
            $kel  = $query->selectMax('id_kelurahan', 'id')->where(['id_kabupaten' => $id_kabupaten, 'id_provinsi' => $id_provinsi, 'id_kecamatan' => $id_kecamatan])->get()->getRow();
            $data = [
                "id_provinsi" => $id_provinsi,
                "id_kabupaten" => $id_kabupaten,
                "id_kecamatan" => $id_kecamatan,
                "id_kelurahan" => intval($kel->id) + 1,
                "nama" => $nama ? $nama : 'not found',
                "created_by" => 1,
                "modified_by" => 1
            ];
            $query->insert($data);
            return intval($kel->id) + 1;
        }
    }
}

if (!function_exists('cekJenisKapal')) {
    function cekJenisKapal($nama)
    {
        $query = new JenisKapalModel();
        $jenis = $query->where('nama', $nama)->get()->getRow();
        if (!empty($jenis->id)) {
            return $jenis->id;
        } else {
            $data = [
                "nama" => $nama,
                "created_by" => 1,
                "modified_by" => 1
            ];
            $query->insert($data);
            return $query->insertID;
        }
    }
}


if (!function_exists('cekUkuranKapal')) {
    function cekUkuranKapal($batas_atas, $batas_bawah)
    {
        $query = new UkuranKapalModel();
        $jenis = $query->where(['batas_atas' => $batas_atas, 'batas_bawah' => $batas_bawah])->get()->getRow();
        if (!empty($jenis->id)) {
            return $jenis->id;
        } else {
            $data = [
                "batas_atas" => $batas_atas,
                "batas_bawah" => $batas_bawah,
                "created_by" => 1,
                "modified_by" => 1
            ];
            $query->insert($data);
            return $query->insertID;
        }
    }
}

if (!function_exists('cekAlatTangkap')) {
    function cekAlatTangkap($alat_tangkap)
    {
        $query = new AlatTangkapModel();
        $alatTangkap = $query->where(['nama' => $alat_tangkap])->get()->getRow();
        if (!empty($alatTangkap->id)) {
            return $alatTangkap->id;
        } else {
            $data = [
                "nama" => $alat_tangkap,
                "created_by" => 1,
                "modified_by" => 1
            ];
            $query->insert($data);
            return $query->insertID;
        }
    }
}

if (!function_exists('cekKbli')) {
    function cekKbli($name, $desc)
    {
        $query = new KbliModel();
        $cek = $query->where(['id' => $name])->get()->getRow();
        if (!empty($cek->id)) {
            return $cek->id;
        } else {
            $data = [
                "id" => $name,
                "name" => $desc,
                "description" => $desc,
                "created_by" => 1,
                "modified_by" => 1
            ];
            $query->insert($data);
            return $name;
        }
    }
}

if (!function_exists('cekDpi')) {
    function cekDpi($name)
    {
        $query = new DaerahPenangkapanIkanModel();
        $cek = $query->where(['name' => $name])->get()->getRow();
        if (!empty($cek->id)) {
            return $cek->id;
        } else {
            $data = [
                "name" => $name,
                "description" => $name,
                "created_by" => 1,
                "modified_by" => 1
            ];
            $query->insert($data);
            return  $query->insertID;
        }
    }
}

if (!function_exists('cekPelabuhanPangkalan')) {
    function cekPelabuhanPangkalan($name)
    {
        $query = new PelabuhanPangkalanModel();
        $cek = $query->where(['name' => $name])->get()->getRow();
        if (!empty($cek->id)) {
            return $cek->id;
        } else {
            $data = [
                "name" => $name,
                "description" => $name,
                "created_by" => 1,
                "modified_by" => 1
            ];
            $query->insert($data);
            return  $query->insertID;
        }
    }
}

if (!function_exists('cekAlokasiPelabuhanMuat')) {
    function cekAlokasiPelabuhanMuat($name)
    {
        $query = new PelabuhanModel();
        $cek = $query->where(['name' => $name])->get()->getRow();
        if (!empty($cek->id)) {
            return $cek->id;
        } else {
            $data = [
                "name" => $name,
                "created_by" => 1,
                "modified_by" => 1
            ];
            $query->insert($data);
            return  $query->insertID;
        }
    }
}

if (!function_exists('cekAlokasiPelabuhanTujuan')) {
    function cekAlokasiPelabuhanTujuan($name)
    {
        $query = new PelabuhanNegaraTujuanModel();
        $cek = $query->where(['name' => $name])->get()->getRow();
        if (!empty($cek->id)) {
            return $cek->id;
        } else {
            $data = [
                "name" => $name,
                "created_by" => 1,
                "modified_by" => 1
            ];
            $query->insert($data);
            return  $query->insertID;
        }
    }
}

if (!function_exists('cekSiup')) {
    function cekSiup($siup)
    {
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $res = $client->request('GET', 'https://perizinan.kkp.go.id/silat/eservices/webservice/query?target=kapi_siup_aktif&pass=pdk@0816&q1=' . $siup . '&ver=2', []);
        $resp = json_decode($res->getBody());

        if ($resp->status == 'ok') {
            if (count($resp->data) > 1) {
                $response = [
                    'status' => 400,
                    'error' => true,
                    'message' => "Failed ",
                    'data' => []
                ];
            } else {
                $response = $resp;
            }
        } else {
            $response = [
                'status' => 400,
                'error' => true,
                'message' => "Failed ",
                'data' => []

            ];
        }
        return $response;
    }
}

if (!function_exists('cekNib')) {
    function cekNib($nib, $nama_pemilik)
    {
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $res = $client->request('GET', 'https://perizinan.kkp.go.id/silat/eservices/webservice/query?target=kapi_simkada_siup_aktif&pass=pdk@0816&q1=' . $nama_pemilik . '&q2=' . $nib . '&Ver=2', []);
        $resp = json_decode($res->getBody()->getContents());
        // var_dump($resp);
        // die;

        if (count($resp) > 1) {
            $response = [
                'status' => 400,
                'error' => true,
                'message' => "Failed ",
                'data' => []
            ];
        } else {
            $response = [
                'status' => 400,
                'error' => true,
                'message' => "Failed ",
                'data' => $resp
            ];
        }
        return $response;
    }
}

if (!function_exists('cekDownload')) {
    function cekDownload($url, $path)
    {
        $url = $url;

        // Initialize the cURL session
        $ch = curl_init($url);

        // Initialize directory name where
        // file will be save
        $dir = $path;

        // Use basename() function to return
        // the base name of file
        $file_name = basename($url);

        // Save file into file location
        $save_file_loc = $dir . $file_name;

        // Open file
        $fp = fopen($save_file_loc, 'wb');

        // It set an option for a cURL transfer
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        // Perform a cURL session
        curl_exec($ch);

        // Closes a cURL session and frees all resources
        curl_close($ch);

        // Close file
        fclose($fp);
        return $file_name;
    }
}

if (!function_exists('Download')) {
    function Download($endpoint, $path, $siup)
    {
        $path = $path . $siup . '.pdf';
        $file_download = curl_init();

        curl_setopt($file_download, CURLOPT_URL, $endpoint);
        curl_setopt($file_download, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($file_download, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($file_download, CURLOPT_AUTOREFERER, true);
        $result = curl_exec($file_download);
        file_put_contents($path, $result);
        return $siup . '.pdf';
    }
}

if (!function_exists('cekNamaKapal')) {
    function cekNamaKapal($namaKapal, $nomor_register)
    {
        $nama = new PersetujuanNamaKapalModel();
        $kapal = $nama->orWhere(['nama_kapal' => $namaKapal, 'nomor_registrasi' => $nomor_register])->first();
        if ($kapal) {
            return true;
        } else {
            return false;
        }
    }
}

if (!function_exists('cekNomorRegister')) {
    function cekNomorRegister($table, $nomor_register)
    {
        $db = \Config\Database::connect();
        $kapal = $db->table($table)->where(['nomor_registrasi' => $nomor_register])->get()->getFirstRow();
        if ($kapal) {
            return true;
        } else {
            return false;
        }
    }
}

function generateRandomStrings($length = 10)
{
    return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
}

if (!function_exists('historyProses')) {
    function historyProses($nomorRegister, $nomor, $tipe, $status, $desc = null, $reason = null, $pemilik_usaha = null, $to = null)
    {
        $log = new PengajuanShiftModel();
        $data = [
            'nomor_registrasi' => $nomorRegister,
            'nomor' => $nomor,
            'user_id' => getUserid(),
            'tipe' => $tipe,
            'status' => $status,
            'description' => $desc,
            'reason' => $reason,
            'pemilik_usaha' => $pemilik_usaha,
            'to' => $to
        ];

        $options = array(
            'cluster' => 'ap1',
            'useTLS' => true
        );
        $pusher = new Pusher(
            '0473e8fbe360116ba2de',
            'c186f88a3a85cd6d2468',
            '1327687',
            $options
        );

        $datas['message'] = $desc;
        $datas['to'] = $to;
        $datas['pemilik_usaha'] = $pemilik_usaha;
        $datas['tipe'] = 'notif';

        $pusher->trigger('my-channel', 'my-event', $datas);

        return $log->insert($data);
    }
}
if (!function_exists('getSessionID')) {
    function getSessionID($user_id)
    {
        session_regenerate_id();
        $update = new UserModel();
        return $update->update($user_id, ['sessionID' => session_id()]);
    }
}

if (!function_exists('connectionID')) {
    function connectionID($user_id, $status)
    {
        $update = new UserModel();
        return $update->update($user_id, ['connectionID' => $status]);
    }
}

if (!function_exists('getData')) {
    function getData($table, $where = [])
    {
        $db = db_connect();
        if ($table != 'persetujuan_nama_kapal') {
            return $db->table($table)
                ->select('p.nomor_registrasi,p.created_by as pemilik')
                ->join('persetujuan_nama_kapal p', 'p.nomor_registrasi=' . $table . '.nomor_registrasi', 'left')
                ->where($where)
                ->get()->getFirstRow();
        } else {
            return $db->table($table)
                ->select($table . '.nomor_registrasi,' . $table . '.created_by as pemilik')
                ->where($where)
                ->get()->getFirstRow();
        }
    }
}
