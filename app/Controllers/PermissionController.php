<?php

namespace App\Controllers;

use App\Entities\Collection;
use App\Models\PermissionModel;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\RESTful\ResourceController;

class PermissionController extends ResourceController
{

    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        //--------------------------------------------------------------------
        // Preload any models, libraries, etc, here.
        //--------------------------------------------------------------------
        // E.g.:
        // $this->session = \Config\Services::session();
        $this->permissions = new PermissionModel();
        helper('Auth');
    }
    // public function __construct()
    // {

    //     $this->permissions = new PermissionModel();
    //     helper('Auth');
    // }
    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    public function index()
    {

        if (!hasPermission('manage_permission')) {
            return $this->respond([
                'code' => 403,
                'status' => 'fail',
                'message' => 'You don\'t have permission to access the data'
            ], 403);
        }

        $start = $this->request->getGet('start');
        $length = $this->request->getGet('length');
        $search = $this->request->getGet('search');

        return $this->respond(Collection::datatable(
            $this->permissions->getResource($search)->limit($length, $start)->get()->getResultObject(),
            $this->permissions->getResource()->countAllResults(),
            $this->permissions->getResource($search)->countAllResults()
        ));
    }

    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    public function getPermissionGroup()
    {

        if (!hasPermission('manage_permission')) {
            return $this->respond([
                'code' => 403,
                'status' => 'fail',
                'message' => 'You don\'t have permission to access the data'
            ], 403);
        }

        return $this->respond(
            $this->permissions->getPermissionsGroup()
        );
    }

    /**
     * Return the properties of a resource object
     *
     * @return mixed
     */
    public function show($id = null)
    {
        //
    }

    /**
     * Return a new resource object, with default properties
     *
     * @return mixed
     */
    public function new()
    {
        //
    }

    public function store()
    {

        if (!hasPermission('manage_permission')) {
            return $this->respond([
                'code' => 403,
                'status' => 'fail',
                'message' => 'You don\'t have permission to access the data'
            ], 403);
        }
        $rules = [
            "name" => "required",
        ];

        $messages = [
            "name" => [
                "required" => "name is required"
            ],
        ];

        if (!$this->validate($rules, $messages)) {

            $response = [
                'code' => 500,
                'error' => true,
                'message' => $this->validator->getErrors()
            ];

            return $this->respondCreated($response);
        } else {

            $save_data = array(
                'name' => $this->request->getPost('name'),
                'description' => $this->request->getPost('description'),

            );
            $create = $this->permissions->insert($save_data);
            if ($create) {
                $response = [
                    'code' => 201,
                    'error' => false,
                    'id' => $create,
                    'message' => "Success Insert Permission"
                ];
                return $this->respondCreated($response);
            } else {
                $response = [
                    'code' => 400,
                    'error' => true,
                    'message' => "Failed Insert"
                ];
                return $this->respondCreated($response);
            }
        }
    }

    public function edits()
    {

        if (!hasPermission('manage_permission')) {
            return $this->respond([
                'code' => 403,
                'status' => 'fail',
                'message' => 'You don\'t have permission to access the data'
            ], 403);
        }
        $id = $this->request->getPost('id');
        $rules = [
            "id" => "required",
        ];
        $messages = [
            "id" => [
                "required" => "id is required"
            ],
        ];

        if (!$this->validate($rules, $messages)) {
            $response = [
                'status' => 500,
                'error' => true,
                'message' => $this->validator->getErrors()
            ];
            return $this->respondCreated($response);
        } else {

            $update_data = array(
                'name' => $this->request->getPost('name'),
                'description' => $this->request->getPost('description'),

            );
            $update = $this->permissions->update($id, $update_data);
            if ($update) {
                $response = [
                    'status' => 201,
                    'error' => false,
                    'message' => "Success Update"
                ];
                return $this->respondCreated($response);
            } else {
                $response = [
                    'status' => 400,
                    'error' => true,
                    'message' => "Failed Update"
                ];
                return $this->respondCreated($response);
            }
        }
    }

    /**
     * Add or update a model resource, from "posted" properties
     *
     * @return mixed
     */
    public function update($id = null)
    {
        //
    }

    public function destroy($id)
    {
        if (!hasPermission('manage_permission')) {
            return $this->respond([
                'code' => 403,
                'status' => 'fail',
                'message' => 'You don\'t have permission to access the data'
            ], 403);
        }

        $delete = $this->permissions->delete($id);

        if ($delete) {
            $response = [
                'status' => 201,
                'error' => false,
                'message' => "Success Delete"
            ];
            return $this->respondCreated($response);
        } else {
            $response = [
                'status' => 400,
                'error' => true,
                'message' => "Failed Delete"
            ];
            return $this->respondCreated($response);
        }
    }
}
