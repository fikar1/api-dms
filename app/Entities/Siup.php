<?php

namespace App\Entities;

use App\Models\UserModel;
use CodeIgniter\Entity\Entity;
use RuntimeException;

class Siup extends Entity{

    protected $user;

    protected $provinsi;

    public function getUser(){
        // if (empty($this->id)) {
        //     throw new RuntimeException('Users must be created before getting siup.');
        // }

        $this->user = model(UserModel::class)->where($this->id)->first();

        return $this->user;
    }
}