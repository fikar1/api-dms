<?php

namespace App\Libraries;

use App\Models\UserModel;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

use Entity\User;
use Entity\Authtoken;
use Illuminate\Support\Facades\Event;
use Socket;

class Websocketserver implements MessageComponentInterface
{
    protected $clients;

    public function updateClient(Event $event)
    {
        $user = $event->getUser(); //@JA - Get reference to the user the event is for.

        echo "userid=" . $user->getId() . "\n";
        echo "busy=" . ($user->getBusy() == false ? "0" : "1") . "\n";
        echo "active=" . ($user->getActive() == false ? "0" : "1") . "\n";

        $json["busy"]    = ($user->getBusy() == false ? "0" : "1");
        $json["active"]  = ($user->getActive() == false ? "0" : "1");

        $msg = json_encode($json);

        foreach ($user->getSockets() as $socket) {
            $connectionid = $socket->getConnectionid();
            echo "Sending For ConnectionID:" . $connectionid . "\n";
            if (isset($this->clients[$connectionid])) {
                $client = $this->clients[$connectionid];
                $client->send($msg);
            } else {
                echo "Client is no longer connected for this Connection ID:" . $connectionid . "\n";
            }
        }
    }

    public function __construct()
    {
        $this->clients = array();
    }

    public function onOpen(ConnectionInterface $conn)
    {
        // Store the new connection to send messages to later
        $this->clients[$conn->resourceId] = $conn;
        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        global $entityManager;

        echo sprintf('Connection %d sending message "%s"' . "\n", $from->resourceId, $msg);

        //@JA - First step is to decode the message coming from the client.  Use token to identify the user (from cookie or local storage)
        //@JA - Format is JSON {token:58d8beeb0ada3:4ffbd272a1703a59ad82cddc2f592685135b09f2,message:register}
        $json = json_decode($msg, true);
        //echo 'json='.print_r($json,true)."\n";
        if ($json["message"] == "register") {
            echo "Registering with server...\n";

            $parts = explode(":", $json["token"]);

            $selector = $parts[0];
            $validator = $parts[1];

            //@JA - Look up records in the database by selector.
            $tokens = $entityManager->getRepository('Entity\Authtoken')->findBy(array('selector' => $selector, 'token' => hash('sha256', $validator)));

            if (count($tokens) > 0) {
                $user = $tokens[0]->getUser();
                echo "User ID:" . $user->getId() . " Registered from given token\n";
                $socket = new Socket();
                $socket->setUser($user);
                $socket->setConnectionid($from->resourceId);
                $socket->setDatecreated(new \Datetime());

                $entityManager->persist($socket);
                $entityManager->flush();
            } else {
                echo "No user found from the given cookie token\n";
            }
        } else {
            echo "Unknown Message...\n";
        }
    }

    public function onClose(ConnectionInterface $conn)
    {
        global $entityManager;

        // The connection is closed, remove it, as we can no longer send it messages
        unset($this->clients[$conn->resourceId]);

        //@JA - We need to clean up the database of any loose ends as well so it doesn't get full with loose data
        $socketResults = $entityManager->getRepository('Entity\Socket')->findBy(array('connectionid' => $conn->resourceId));
        if (count($socketResults) > 0) {
            $socket = $socketResults[0];
            $entityManager->remove($socket);
            $entityManager->flush();
            echo "Socket Entity For Connection ID:" . $conn->resourceId . " Removed\n";
        } else {
            echo "Was no socket info to remove from database??\n";
        }

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }
}
