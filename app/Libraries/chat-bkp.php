<?php
namespace App\Libraries;

use App\Models\ConnectionsModel;
use App\Models\MessageModel;
use App\Models\UserModel;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class Chat implements MessageComponentInterface {
    protected $clients;
    public $userObj,$mess, $userData;
    public function __construct() {
         // save configuration
        $this->clients = new \SplObjectStorage;
    }

    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later

        $queryString = $conn->httpRequest->getUri()->getQuery();
        $query = explode('=',$queryString); 

        $this->userObj = new UserModel();
        $conModel = new ConnectionsModel();

        $data = $this->userObj->getUserBySession($query[1]);

        $this->userData = $data;
        $conn->userData = $data;
        $this->clients->attach($conn);

        $conModel->where('c_user_id', $data->id)->delete();
        $conData = [
                'c_user_id' => $data->id,
                'c_resource_id' => $conn->resourceId,
                'c_name' => $data->username
        ];

        $conModel->save($conData);
        $users = $conModel->get()->getFirstRow();
        // $users = ['users' => $users];

        $users['sentBy']= getUserid();
        $users['sentTo']= $users->c_user_id;
        $users['message']= "offline";
        $users['type']= "message";
        $users['avatar']= "";
        $users['username']= $users->c_name;
        foreach ($this->clients as $client) {
            $client->send(json_encode($users));
        }


        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        $numRecv = count($this->clients) - 1;
        echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
            , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');
        
        $this->userObj = new UserModel();
        $this->mess = new MessageModel();
        $data = json_decode($msg,true);
        $sentTo  = $this->userObj->userData($data['sentTo']);
        $this->mess->insert([
            'message'   => $data['message'],
            'sentBy'    => $from->userData->id,
            'sentTo'    => $data['sentTo']
        ]);

        $send['sentBy']= $from->userData->id;
        $send['sentTo']= $sentTo->id;
        $send['message']= $data['message'];
        $send['type']= $data['type'];
        $send['avatar']= $from->userData->avatar;
        $send['username']= $from->userData->username;

        foreach ($this->clients as $client) {
            if ($from !== $client) {
                $client->send(json_encode($send));
                // The sender is not the receiver, send to each client connected
                if($client->resourceId  == $sentTo->connectionID || $from == $client){
                    $client->send(json_encode($send));
                }
            }
        }
    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
       
        $this->clients->detach($conn);

        $conModel = new ConnectionsModel();
        $conModel->where('c_resource_id', $conn->resourceId)->delete();
        $users = $conModel->findAll();
        $users = ['users' => $users];
        foreach ($this->clients as $client) {
            $client->send(json_encode($users));
        }

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }
}