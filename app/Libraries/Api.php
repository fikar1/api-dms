<?php

namespace App\Libraries;

use GuzzleHttp\Client;

class Api {
    
    private $client;
    function __construct() {
        $this->client = new Client();
    }


    public function url($url){

        $this->client = new Client([
            'base_uri' => $url,
        ]);
        return $this->client ;
    }

    public function post($url , array $data, $type, array $header){
        $response = $this->client->post($url,[
            $type => 
                    $data
                ,
            'header' => $header,
            'verify' => false
        ]);
        return $response;
    }

    public function get($url , array $data, $type, array $header){
        $response = $this->client->get($url,[
            $type => 
                    $data
                ,
            'header' => $header,
            'verify' => false
        ]);
        return $response;
    }

    public function put($url , array $data, $type,array $header){
        $response = $this->client->put($url,[
            $type => 
                    $data
                ,
                'header' => $header
        ]);
        return $response;
    }

    
}
