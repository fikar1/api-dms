<?php
namespace App\Libraries;

use Exception;
use Ratchet\Http\HttpServerInterface;
use CodeIgniter\HTTP\Response;
use Psr\Http\Message\RequestInterface ;
use Ratchet\ConnectionInterface;

class Htmlserver implements HttpServerInterface{
    protected $response;

    public function onOpen(ConnectionInterface $conn, ?RequestInterface $request = null)
    {
        global $dispatcher;
        $this->response = new Response( 200, [
            'Content-Type' => 'text/html; charset=utf-8',
        ] );

        $query = $request->getUri();
        parse_str($query, $get_array);//@JA - Convert query to variables in an array

        $json = json_encode($get_array);//@JA - Encode to JSON

        //@JA - Send JSON for what you want to do and the token representing the user & therefore connected user as well.
        $event = new ClientEvent($json);
        $dispatcher->dispatch("websocketserver.updateclient",$event);

        $this->response->setBody('{"message":"Successfully sent message to websocket server")');
        echo "HTTP Connection Triggered\n";
        $this->close( $conn );
    }

    public function onClose(ConnectionInterface $conn)
    {
        echo "HTTP Connection Ended\n";
    }

    public function onError(ConnectionInterface $conn, Exception $e)
    {
        echo "HTTP Connection Error\n";
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        echo "HTTP Connection Message\n";
    }

    protected function close( ConnectionInterface $conn ) {
        $conn->send( $this->response );
        $conn->close();
    }
}